describe("jskanren", function() {

    describe("base", function() {

        var x,y,z;
        var f1,f2,f3;

        beforeEach(function() {
            //player = new Player();
            f1 = function(x) { return [x+1]; }
            f2 = function(x) { return [x*2]; }
            f3 = function(x) { return [x-1]; }
            x = lvar("x");
            y = lvar("y");
            z = lvar("z");
        });

        it("should create and maintain logic vars", function() {
            var va = lvar();
            var vb = lvar("vb");
            expect(va).toMatch(/^#\d/);
            expect(vb).toMatch(/^#vb\d/);
            expect(true).toEqual(is_lvar(va));
            expect(true).toEqual(is_lvar(vb));
        });

        it("should lookup", function() {
            var obj1 = {}, obj2 = {}, obj3 = {};
            expect(x).toEqual( lookup(x, {}) );
            obj1[x] = 4;
            expect(4).toEqual( lookup(x, obj1) );
            obj2[x] = y;
            obj2[y] = 5;
            expect(5).toEqual( lookup(x, obj2) );
            obj3[z] = 1;
            expect(x).toEqual( lookup(x, obj3) );
        });


        it("should unify", function() {
            expect({}).toEqual( unify(1, 1, {}) );
            expect(false).toEqual( unify(1, 2, {}) );

            expect({ "#x": 1 }).toEqual( unify("#x", 1, {}) );
            expect({ "#x": 1 }).toEqual( unify(1, "#x", {}) );
            expect({ "#x": 1 }).toEqual( unify(1, "#x", { "#x": 1 }) );

            expect({ "#x": "#y" }).toEqual( unify("#x", "#y", {}) );
            expect({ "#y": "#x" }).toEqual( unify("#y", "#x", {}) );

            expect({ "#y": [1] }).toEqual( unify("#y", [1], {}) );
            expect({ "#y": [1] }).toEqual( unify([1], "#y", {}) );

            expect({ "#y": [] }).toEqual( unify("#y", [], {}) );

            expect({ "#x": 1 }).toEqual( unify(["#x"], [1], {}) );
            expect({ "#x": 1 }).toEqual( unify([1], ["#x"], {}) );

            expect(false).toEqual( unify(["#x"], [1, 2], {}) );

            expect({ "#x": 3, "#z": 3, "#y": [] }).toEqual( unify(pair("#x", "#y"), ["#z"], { "#z": 3 }) );
            expect({ "#y": [2, 3], "#x": 1 }).toEqual( unify(pair("#x", "#y"), [1, 2, 3], {}) );
        });

        it("should succeed", function() {
            expect([1]).toEqual( succeed(1) );
        });

        it("should fail", function() {
            expect([]).toEqual( fail(1) );
            expect([]).toEqual( fail([1,2]) );
        });

        it("should disj", function() {
            //f1(x) -> [x + 1]
            //f2(x) -> [x * 2]
            //f3(x) -> [x - 1]
            var f = disj(
                disj(fail, succeed),
                conj( 
                    disj(function (x) { return succeed(x + 1) },
                         function (x) { return succeed(x + 10) }),
                    disj(succeed, succeed)
                )
            );
            expect([100,101,101,110,110], f(100) )
            expect([3,4]).toEqual( disj2(f1, f2)(2) );
            expect([3,4]).toEqual( disj(f1, f2)(2) );
            expect([3,4,1]).toEqual( disj(f1, f2, f3)(2) );
        });

        it("should conj", function() {
            //f1(x) -> [x + 1]
            //f2(x) -> [x * 2]
            //f3(x) -> [x - 1]
            expect([3]).toEqual( conj(f1)(2) );
            expect([6]).toEqual( conj2(f1, f2)(2) );
            expect([6]).toEqual( conj(f1, f2)(2) );
            expect([5]).toEqual( conj(f1, f2, f3)(2) );
            expect([5]).toEqual( conj([f1, f2, f3])(2) );
            expect([3]).toEqual( conj(f3, f2, f1)(2) );
            expect([3]).toEqual( conj(f3, conj(f2, f1))(2) );
        });


    });

    describe("logic", function() {

        it("should eqo", function() {
            expect([{ "#x" : 1  }]).toEqual( eqo("#x", 1)({}) );
            //expect([{ "#x" : 1  }]).toEqual( conj(eqo("#x", "#y"), eqo("#y", 1))({}) );
        });

        it("should existo", function() {
            expect([{}]).toEqual( existo(2, [1, 2, 3])({}) );
            expect([{"#x" : 1}, {"#x" : 2}, {"#x" : 3}]).toEqual( existo("#x", [1, 2, 3])({}) );
            //expect([{"#x" : 1}, {},  {"#x" : 1}]).toEqual( existo("#x", [1, "#x", 3])({}) );
        });

        it("should commono", function() {
            expect([1,2]).toEqual( run( "#q", commono("#q", [1, 2, 4, 5], [1, 2, 3]) ) );
            expect([]).toEqual( run( "#q", commono([4, 5], [2, 3]) ) );
        });

        it("should conso", function() {
            expect([[1,2,3]]).toEqual( run( "#q", conso( 1, [2, 3], "#q")) );
            expect([[ 1,2,3 ]]).toEqual(
                run( "#q",
                    eqo( "#q", [ "#x", "#y" ]),
                    conso( "#x", "#y", [1, 2, 3]) )
            );
        });

        it("should appendo", function() {
            expect([[4,5]]).toEqual( run( "#q", appendo([1, 2, 3], "#q",  [1, 2, 3, 4, 5]) ) );
        });

        it("should membero", function() {
            expect(["#q"]).toEqual( run( "#q", membero(1, [1, 2, 3])) );
            expect([]).toEqual( run( "#q", membero(4, [1, 2, 3])) );
        });
    });

    describe("zebra puzzle", function() {

        it("should righto", function() {
            expect(["#q"]).toEqual( run("#q", righto(1,2, [1,2,3,4])) );
            expect(["#q"]).toEqual( run("#q", righto(2,3, [1,2,3,4])) );
            expect([]).toEqual( run("#q", righto(1,4, [1,2,3,4])) );
            expect([]).toEqual( run("#q", righto(2,1, [1,2,3,4])) );
        });

        it("should solve einstein zebra puzzle", function() {
            var expected = [
                'norwegian', 'kools', '_', 'fox', 'yellow',
                'ukrainian', 'chesterfields', 'tea', 'horse', 'blue',
                'englishman', 'oldgolds', 'milk', 'snails', 'red',
                'spaniard', 'lucky-strikes', 'oj', 'dog', '_',
                'japanese', 'parliaments', 'coffee', '_', 'green' ];
            console.time('zebrao time');
            var result = run("#q", zebrao("#q"))[0];
            console.timeEnd('zebrao time');
            expect(expected).toEqual( result.map(function (e) { return is_lvar(e) ? "_" : e }) );
        });

    });

});

