

function righto(x, y, l) {
    var t = lvar("t");
    return disj(
        conj(
            conso(x, t, l),
            conso(y, lvar(), t)
        ),
        conj(
            conso(lvar(), t, l),
            function(s) { return righto(x, y, t)(s) }
        )
    );
}

function nexto(x, y, l) {
    return disj(
        righto(x, y, l),
        righto(y, x, l)
    );
}

function zebrao(q) {
    return conj(
        eqo([lvar(), lvar(), [lvar(), lvar(), "milk", lvar(), lvar()], lvar(), lvar()], q),
        firsto(q, ["norwegian", lvar(), lvar(), lvar(), lvar()]),
        nexto(["norwegian", lvar(), lvar(), lvar(), lvar()],
              [lvar(), lvar(), lvar(), lvar(), "blue"], q),
        righto( ["norwegian", lvar(), lvar(), lvar(), lvar()],
                [lvar(), lvar(), lvar(), lvar(), "blue"], q),
        membero(["englishman", lvar(), lvar(), lvar(), "red"], q),
        membero([lvar(), "kools", lvar(), lvar(), "yellow"], q),
        membero(["spaniard", lvar(), lvar(), "dog", lvar()], q),
        membero([lvar(), lvar(), "coffee", lvar(), "green"], q),
        membero(["ukrainian", lvar(), "tea", lvar(), lvar()], q),
        membero([lvar(), "lucky-strikes", "oj", lvar(), lvar()], q),
        membero(["japanese", "parliaments", lvar(), lvar(), lvar()], q),
        membero([lvar(), "oldgolds", lvar(), "snails", lvar()], q),
        nexto( [lvar(), lvar(), lvar(), "horse", lvar()],
               [lvar(), "kools", lvar(), lvar(), lvar()], q),
        nexto( [lvar(), lvar(), lvar(), "fox", lvar()],
               [lvar(), "chesterfields", lvar(), lvar(), lvar()], q )
    );
}

