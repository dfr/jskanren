/*
 *
 * http://jsfiddle.net/2nYjZ/
 *
 * */
"use strict";
var LVARC = 1;

function cloneObject(obj) {
    function Clone(){}
    Clone.prototype=obj;
    return new Clone();
}

function lvar(v) {
    v = v || "";
    return "#" + v + LVARC++;
}

function is_lvar(v) {
    return typeof v == "string" && v.substr(0,1) == "#";
}

function pair(a,b) {
    return { first: a, second: b };
}

function flatten(array) {
    return [].concat.apply([], array);
}

function lookup(v, s) {
    if(! is_lvar(v)) {
        return v;
    }
    else {
        return (v in s) ? lookup(s[v], s) : v
    }
}

function deep_lookup(v, s) {
    var v = lookup(v, s);
    if(is_pair_or_list(v)) {
        return flatten( [ deep_lookup( first(v), s ), deep_lookup( rest(v), s ) ] );
    }
    else {
        return v;
    }
}

function non_empty_list(l) {
    return Array.isArray(l) && l.length > 0
}

function is_empty_list(l) {
    return Array.isArray(l) && l.length === 0
}

function is_list(l) {
    return Array.isArray(l)
}

function is_pair(obj) {
    return obj.hasOwnProperty("first") && obj.hasOwnProperty("second");
}

function is_pair_or_list(obj) {
    return is_pair(obj) || non_empty_list(obj);
}

function first(obj) {
    return is_pair(obj) ? obj.first : obj[0];
}

function rest(obj) {
    return is_pair(obj) ? obj.second : obj.slice(1);
}


function unify(t1, t2, s) {
    var r = unify2(t1, t2, s);
    //console.log("unify(%s,%s,%s) => %s", JSON.stringify(t1), JSON.stringify(t2), JSON.stringify(s), JSON.stringify(r));
    return r;
}

function unify2(t1, t2, s, d) {
    //if(JSON.stringify(s).length > 1000) {
        //console.log("!!!inf loop!!!");
        //return false;
    //}
    if(!s) return false;
    t1 = lookup(t1, s);
    t2 = lookup(t2, s);
    if(is_empty_list(t1) && is_empty_list(t2)) return s;
    if(t1 === t2) return s;
    if(is_lvar(t1)) {
        var new_s = cloneObject(s);
        new_s[t1] = t2;
        return new_s;
    }
    if(is_lvar(t2)) {
        var new_s = cloneObject(s);
        new_s[t2] = t1;
        return new_s;
    }
    if(is_pair_or_list(t1) && is_pair_or_list(t2)) {
        var s_head = unify(first(t1), first(t2),s);
        return s_head ? unify(rest(t1), rest(t2),s_head) : s_head;
    }
    //if(t1 == t2) return s;
    // case when none of vars is list or lvar
    return false;
}

function succeed(x) {
    if(x == undefined) throw("succeed need 1 arg");
    return [x];
}

function fail(x) {
    return [];
}

function disj2(f1, f2) {
    return function(x) {
        return f1(x).concat( f2(x) );
    };
}

function disj() {
    var args = Array.prototype.slice.call(arguments);
    return args.reduce(disj2);
}

function bind(mv, f) {
    return flatten(mv.map(f))
}

function conj2(f1, f2) {
    return function(x) {
        return bind( f1(x), f2 );
    };
}

function conj() {
    var args = flatten( Array.prototype.slice.call(arguments) );
    return args.reduce(conj2);
}

function run() {
    var args = Array.prototype.slice.call(arguments);
    var lvar = first(args);
    return conj( rest(args) )({}).map(function (s) { return deep_lookup(lvar, s) });
}

/* Logic system */

function eqo(t1, t2) {
    return function(s) {
        var s1 = unify(t1, t2, s);
        return s1 ? succeed(s1) : fail(s);
    };
}

/* existo(x, l) succeeds if x is member of l */
function existo(v, lst) {
    if(!is_list(lst) || lst.length == 0) return fail;
    return disj( eqo(v, lst[0]), existo(v, lst.slice(1)) );
}

function commono(x, lst1, lst2) {
    return conj(existo(x,lst1), existo(x, lst2));
}

/* conso(a, b, lst) is a goal that succeeds if in the current state
 of the world, pair(a, b) is the same as l */
function conso(a, b, lst) {
    //console.log("conso(%o,%o,%o)", a, b, lst);
    return eqo( pair(a, b), lst );
}

/*  appendo(l1, l2, l3) holds if the list l3 is the
    concatenation of lists l1 and l2. */
function appendo(l1, l2, l3) {
    //console.log("appendo(%o,%o,%o)", l1, l2, l3);
    var h = lvar("h");
    var t = lvar("t");
    var p = lvar("p");
    return disj(
        conj( eqo(l1, []), eqo(l2, l3) ),
        conj( conso(h, t, l1),
              conso(h, p, l3),
              function(s) { return appendo(t, l2, p)(s) }
        )
    );
}

/* membero(x, l) succeeds if x is member of l */
function membero(x, l) {
    //console.log("membero(%o,%o)", x, l);
    var h = lvar("h");
    var t = lvar("t");
    return disj(
        conso(x, t, l),
        conj( conso(h, t, l),
              function(s) { return membero(x, t)(s) }
        )
    );
}

/* firsto(l, a) succeeds if l is a first element of a */
function firsto(l, a) {
    return conso(a, lvar(), l);
}

